#
# ClusterModule.py
#

# Konstantinos Chartomatzis

#
# Provides clustering algorithms such as Community Detection
# and Betweenness Centrality
#

import operator
import networkx as nx
import community


class ClusterModule:
    def __init__(self, graph):
        self.graph = graph

    def community_detection(self):
        return community.best_partition(self.graph)

    def printPartitions(self, partition):
        for i in set(partition.values()):
            print "Community", i
            members = [nodes for nodes in partition.keys() if partition[nodes] == i]
            print members

    def centralityDetection(self, graph):
        return nx.betweenness_centrality(self.graph)

    def getTopKNodes(self, centralities, k):
        # a sorted dict with id,centrality_scores sorted
        sorted_c = sorted(centralities.items(), key=operator.itemgetter(1), reverse=True)
        return sorted_c[0:k]  # (id, centrality_score)
