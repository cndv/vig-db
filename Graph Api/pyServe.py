#
# pyServe.py
#
#
# Konstantinos Chartomatzis
#
#
# pyServe Class is responsible for providing all the necessary functionality
# for a web service API.
#


import flask
from GraphSummary import GraphSummary
from Layout import Layout
from pyNeo import Neo4jHandler

app = flask.Flask(__name__)

#
# populates the graph database with some default data
#
@app.route("/resetgraph")
def resetGraph():
    nh = Neo4jHandler()
    nh.resetDB()
    return "reset db"

#
# deletes all the data of the graph
#
@app.route("/deletegraph")
def deleteGraph():
    nh = Neo4jHandler()
    nh.deleteAll()
    return "delete db"

#
# default route
#
@app.route("/")
def health():
    return flask.jsonify({
        "status": "ok"
    })

#
# returns server error 500
#
@app.route("/healthcheck/fail")
def health_fail():
    return flask.jsonify({
        "status": "error"
    }), 500

#
# Returns the complete graph, with parameter the chosen cluster and the chosen layout
# example url : http://0.0.0.0:5000/graphdata?cluster=nodetype&layout=spring
#

@app.route('/graphdata', methods=['GET'])
def graphdata():
    cluster = flask.request.args.get('cluster') # get cluster algorithm
    layout = flask.request.args.get('layout') # get layout algorithm
    nh = Neo4jHandler()
    d = nh.turnObjectsToDictionary()
    layout = Layout(d, layout, cluster)
    dd = layout.produceLayout()
    #
    # Print Stats
    #
    print "nodesLen is", len(dd["nodes"])
    print "edgesLen is", len(dd["edges"])
    print "size of dict is ", len(dd)
    return flask.jsonify(dd)

#
# Returns the Graph Summary of the Graph, with parameters the chosen cluster and layout algorithms
# example url : http://0.0.0.0:5000/graphsummary?cluster=nodetype&layout=spring
#

@app.route('/graphsummary', methods=['GET'])
def graphSummary():
    cluster = "community" # graph summary is based on community detection
    layout = flask.request.args.get('layout')
    nh = Neo4jHandler()
    graph = nh.turnObjectsToDictionary()
    layout = Layout(graph, layout, cluster)
    graph_position = layout.produceLayout()
    summary = GraphSummary(graph_position)
    graph_summary = summary.produceSummaryDict()
    return flask.jsonify(graph_summary)


if __name__ == '__main__':
    app.run(host="0.0.0.0")
