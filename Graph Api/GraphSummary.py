#
# GraphSummary.py
#
# Konstantinos Chartomatzis
#
# Returns The summary of graph based on community detections
#

class GraphSummary:
    def __init__(self, graph):
        self.graph = graph
        self.partitions = graph["partitions"]

    # dict of clusters_centroids
    # "cluster_centroids = [{cluster = 1, nodes = [1,3,4], x=, y=, z= },{...}]
    #
    def getClusterCentroids(self):
        cluster_centroids = {}

        for n in self.graph["nodes"]:
            cluster_id = n["cluster"]
            if cluster_id not in cluster_centroids:
                cluster_centroids[cluster_id] = {}
            if "nodes" not in cluster_centroids[cluster_id]:
                cluster_centroids[cluster_id]["nodes"] = []

            d = {"id": str(n["id"]), "x": n["x"], "y": n["y"], "z": n["z"]}
            cluster_centroids[cluster_id]["nodes"].append(d)

            if "x" in cluster_centroids[cluster_id]:
                cluster_centroids[cluster_id]["x"] += float(n["x"])
            else:
                cluster_centroids[cluster_id]["x"] = float(n["x"])

            if "y" in cluster_centroids[cluster_id]:
                cluster_centroids[cluster_id]["y"] += float(n["y"])
            else:
                cluster_centroids[cluster_id]["y"] = float(n["y"])

            if "z" in cluster_centroids[cluster_id]:
                cluster_centroids[cluster_id]["z"] += float(n["z"])
            else:
                cluster_centroids[cluster_id]["z"] = float(n["z"])

            if "node_types" not in cluster_centroids[cluster_id]:
                cluster_centroids[cluster_id]["node_types"] = []
            if n["type"] not in cluster_centroids[cluster_id]["node_types"]:
                cluster_centroids[cluster_id]["node_types"].append(n["type"])

        # calculate centroid for coords
        for c in cluster_centroids:
            count_of_nodes = len(cluster_centroids[c]["nodes"])
            cluster_centroids[c]["x"] /= float(count_of_nodes)
            cluster_centroids[c]["y"] /= float(count_of_nodes)
            cluster_centroids[c]["z"] /= float(count_of_nodes)

        return cluster_centroids

    def getClusterLinks(self):
        # dict of cluster_links
        # "cluster_links" = {1: {targets = [2,3], weight = 4, edge_types = [] },
        #                   {}
        #                   }
        cluster_links = {}
        for e in self.graph["edges"]:
            source = e['source']
            target = e['target']
            edge_type = e['type']
            source_cluster = self.partitions[source]
            target_cluster = self.partitions[target]
            if source_cluster != target_cluster:
                if source_cluster < target_cluster:
                    self.addTarget(cluster_links, source_cluster, target_cluster, edge_type)
                else:
                    self.addTarget(cluster_links, target_cluster, source_cluster, edge_type)

        return cluster_links

    def addTarget(self, cluster_links, source_cluster, target_cluster, edge_type):
        if source_cluster not in cluster_links:
            cluster_links[source_cluster] = {}
        if "targets" not in cluster_links[source_cluster]:
            cluster_links[source_cluster]['targets'] = []
        if target_cluster not in cluster_links[source_cluster]['targets']:
            cluster_links[source_cluster]['targets'].append(target_cluster)

        if "edge_types" not in cluster_links[source_cluster]:
            cluster_links[source_cluster]['edge_types'] = []
        if edge_type not in cluster_links[source_cluster]['edge_types']:
            cluster_links[source_cluster]['edge_types'].append(edge_type)

        if "weight" not in cluster_links[source_cluster]:
            cluster_links[source_cluster]['weight'] = 1
        else:
            cluster_links[source_cluster]['weight'] += 1

    def produceSummaryDict(self):
        d = {"cluster_centroids": self.getClusterCentroids(), "cluster_links": self.getClusterLinks()}
        return d
