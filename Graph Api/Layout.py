#
# Layout.py
#
# Konstantinos Chartomatzis
#
# Layout Class is responsible for producing the correct layout of the graph
# which may be Force Direct or Spectral. In addition, clusters the data,
# by either using the Community Detection or the Betweeness Centrality algorithm
#


import networkx as nx
from ClusterModule import ClusterModule

class Layout:
    def __init__(self, graph, lt, cl):
        self.graph = graph
        self.lt = lt
        self.cluster = cl

    #
    # returns a dictionary with the nodes, clusters, centralities
    # every node has a vector, and a cluster if community detection has
    # been selected
    #
    def produceLayout(self):
        #
        # initialize graph
        #
        H = nx.Graph()
        for n in self.graph["edges"]:
            H.add_edge(int(n["source"]), int(n["target"]))

        #
        # choose layout for the graph drawing
        #
        if self.lt == "spring":
            graph_pos = nx.spring_layout(H, dim=3)
        elif self.lt == "spectral":
            graph_pos = nx.spectral_layout(H, dim=3)

        #
        # community detection
        #
        clusterManager = ClusterModule(H)
        partitions = clusterManager.community_detection()

        #
        # betweenes centralityu
        #
        if self.cluster == "centrality":
            clusterManager = ClusterModule(H)
            topKcentralities = clusterManager.getTopKNodes(clusterManager.centralityDetection(H), 10)

        #
        # add coordinates and in which cluster a node belongs
        #
        for k in self.graph["nodes"]:
            k["x"] = str(graph_pos[k['id']][0])
            k["y"] = str(graph_pos[k['id']][1])
            k["z"] = str(graph_pos[k['id']][2])
            if self.cluster == "community":
                k["cluster"] = str(partitions[k['id']])

        d = self.graph
        if self.cluster == "community":
            # add the clusters in which every node belongs, to the final dictionary
            d["partitions"] = partitions
        elif self.cluster == "centrality":
            # add the top K central nodes of the graph, to the final dictionary
            centDict = dict(topKcentralities)
            for k, v in centDict.iteritems():
                centDict[k] = str(v)
            d["centralities"] = centDict
        return d
