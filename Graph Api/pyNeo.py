#
# pyNeo.py
#
# Konstantinos Chartomatzis
#
# Neo4jHandler Class
# Establishes a connection with the Neo4j Server
# Runs Cypher queries and returns the corresponding data in a dictionary
#

import json
import string
import random
import sys
from py2neo import Graph, Node, Relationship


class Neo4jHandler:
    def __init__(self):
        # self.graph = Graph()
        #self.graph = Graph("http://viDB:LI8uMUo7FucjtKAR4Wa4@vidb.sb05.stations.graphenedb.com:24789/db/data/")
         self.graph = Graph("http://neo4j:neo4jneo4j@localhost:7474/db/data")

    def id_generator(self, size=6, chars=string.lowercase):
        return ''.join(random.choice(chars) for _ in range(size))

    # random number
    def getRandomInt(self, fromX, toZ):
        return random.randint(fromX, toZ)

    # create some sample data in the graph
    def resetDB(self):
        # sample nodes
        mike = Node("Person", name="Mike")
        bob = Node("Person", name="Bob")
        kostas = Node("Person", name="Kostas")
        john = Node("Boss", name="John")
        nick = Node("Boss", name="Nick")
        alice = Node("Boss", name="Alice")
        # sample relations
        a = Relationship(mike, "KNOWS", kostas)
        b = Relationship(mike, "KNOWS", bob)
        c = Relationship(alice, "HAS", kostas)
        d = Relationship(alice, "HAS", nick)
        e = Relationship(john, "HAS", alice)
        # create the graph
        self.graph.create(a)
        self.graph.create(b)
        self.graph.create(c)
        self.graph.create(d)
        self.graph.create(e)

    # delete all nodes of the graph
    def deleteAll(self):
        deleteQuery = "MATCH (n) OPTIONAL MATCH (n)-[r]-() DELETE n,r "
        self.graph.cypher.execute(deleteQuery)

    # return a dictionary with the nodes and the relations of the graph
    def turnObjectsToDictionary(self):
        nodes = self.getAllNodes()
        relations = self.allRelations()

        try:
            d = {"nodes": []}
            for n in nodes:
                dtmp = {"id": n['id']}
                if n['name'] is None:
                    dtmp["name"] = self.id_generator()
                else:
                    dtmp["name"] = n['name']
                dtmp["type"] = n['type'][0]
                d["nodes"].append(dtmp)
        except:
            e = sys.exc_info()[0]
            print e

        d["edges"] = []
        for r in relations:
            dt = {'source': r[0].start_node._id, 'target': r[0].end_node._id, 'type': r[0].type}
            d['edges'].append(dt)

        return d

    # convert a dictionary to the proper format for a JSON file to be consumed from within
    # a web service
    def dictToJSON(self, d, filename):
        with open(filename, 'w') as fp:
            json.dump(d, fp)

    # get all the nodes of the graph
    def getAllNodes(self):
        query = self.graph.cypher.execute("match (n) return ID(n) as id, n.name AS name, labels(n) as type")
        return query.records

    # get all the relations of the graph
    def allRelations(self):
        query = "start r=rel(*) return r"
        results = self.graph.cypher.execute(query)
        return results.records
