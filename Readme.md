## MSc Dissertation University of Edinburgh ##
## viDb: Graph Database Exploration Tool for the iPad ##
[Video](https://youtu.be/bDp8YML4BTk)

Name: Konstantinos Chartomatzis

## Description

As we enter the era of big data, interconnected data are becoming more and more pervasive. In order to store, manage, and extract knowledge from data, there is a need for new tools regarding data management, but also tools for interpreting the information that data convey. For instance applications such as Social Networks cannot rely on traditional relational database systems. Therefore, there is a continuous shift towards adoption of other choices such as NoSQL systems, and particular graph databases, where graphs are the basic data structure for storing data. Also, it is apparent that there is a request for tools that are simple, require no expert knowledge, and lead to a faster exploration of data through visualization. Visual data exploration can be achieved following three simple processes, providing an overview, zooming and filtering, and requesting details-on-demand (Shneiderman, 1996). This work presents an interactive graph database visualization tool built for the iPad device, called ViGdb. It emphasizes on the task of database exploration by using 3D graphics visualization, and an intuitive user-friendly manipulation of the data, using gestures.
## Tool Versions
###Graph API
- Flask 0.10.1
- NetowrkX 1.10
- Python 2.7

### ViGdb iOS App
- Xcode 6.4
- iOS 8.4 
- OSX 10.10.5
	### Open Source Libraries
	- Carthage
	- SwiftyJSON
	- PKHud
	- CocoaPods