//
//  GraphHandler.swift
//
// Konstantinos Chartomatzis
//
//  Handling Model for graph data
//

import Foundation
import UIKit




func randomCGFloat() -> CGFloat {
    return CGFloat(arc4random()) / CGFloat(UInt32.max)
}


class GraphHandler {
    var graph: GraphModel
    var colorMngr : ColorModel
    var nodeTypeColor: [String:UIColor]
    var edgeColor: [String:UIColor]
    var classColor: [Int:UIColor]
    var clustering: Clustering
    var centralities: [Int:Double]

    
    init (){
        self.graph = GraphModel()
        self.colorMngr = ColorModel()
        self.nodeTypeColor = [String:UIColor]()
        self.edgeColor = [String:UIColor]()
        self.classColor = [Int:UIColor]()
        self.clustering = .Community
        self.centralities = [Int:Double]()
    }
    
    func getRandomColor ()-> UIColor {
        return randomColor(hue: .Random, luminosity: .Bright)
    }
    
    
    func parseCentralities(topCentralities:JSON) {
        println(topCentralities)
        for (cID, v) in topCentralities {
            var nodeId = cID.toInt()
            var score = v.doubleValue
            self.centralities[nodeId!] = score
        }
    }
    
    func parseData(data: AnyObject){
        var g = JSON(data)
        var edges = g["edges"]
        var nodes = g["nodes"]
//        var communityPartitions = g["partitions"]
        if clustering == .Centrality {
            var topCentralities = g["centralities"]
            println(topCentralities)
            self.parseCentralities(topCentralities)
        }
        var source:Int
        var target:Int
        var type:String
        var randColor = Set<UIColor>()

        println("info about graph" )
        println("Number of nodes: \(nodes.count)")
        println("Number of edges \(edges.count)")
        
        //
        // parse edges
        //
        for var i=0; i<edges.count; i++ {
            source = edges[i]["source"].int!
            target = edges[i]["target"].int!
            type = edges[i]["type"].string!
            var r = Relation(source: source, target: target, type: type)
            
            if colorMngr.edgeColor[type] == nil  {
                var rcolor = colorMngr.getColor()
                colorMngr.edgeColor[type] = rcolor
            }
            self.graph.addEdge(r)
        }
        
        
        //
        // parse nodes
        //
        var x = 0.0
        var y = 0.0
        for var i=0; i<nodes.count; i++ {
            
            var name:String = ""
            if nodes[i]["name"].string == nil {
                name = self.randomStringWithLength(10) as String
            }
            else{
                name = nodes[i]["name"].string!
            }

                var temp = [String:String]()
                var id = Int(nodes[i]["id"].int!)
                
                var x = nodes[i]["x"].floatValue
                var y = nodes[i]["y"].floatValue
                var z = nodes[i]["z"].floatValue
                var nodeType = nodes[i]["type"].string!
                var classType = nodes[i]["cluster"].intValue
                if colorMngr.nodeTypeColor[nodeType] == nil  {
                    var rcolor = colorMngr.getColor()
                    colorMngr.nodeTypeColor[nodeType] = rcolor
                }
            
                if colorMngr.classColor[classType] == nil  {
                    var rcolor = colorMngr.getColor()
                    colorMngr.classColor[classType] = rcolor
                }
            
                var node = SphereNode(id: id, name: name, nodeType:nodeType, x: x, y:y, z:z, classType: classType)
                self.graph.addNode(node)
            
        }
    }
    
    func randomStringWithLength (len : Int) -> NSString {
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var randomString : NSMutableString = NSMutableString(capacity: len)
        
        for (var i=0; i < len; i++){
            var length = UInt32 (letters.length)
            var rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.characterAtIndex(Int(rand)))
        }
        
        return randomString
    }
    
}

