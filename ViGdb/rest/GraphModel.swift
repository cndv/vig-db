//
//  Graph.swift
//
// Konstantinos Chartomatzis
//
// Basic Graph Model, Stores nodes and edges
//

import Foundation
        

class GraphModel {
    
    var edges : [Relation]
    var nodes : [Int: SphereNode] // id : sphereNode
    
    init() {
        // perform some initialization here
        edges = [Relation]()
        nodes = [Int: SphereNode]()
    }
    
    func addEdge(e: Relation){
        self.edges.append(e)
    }
    
    func addNode(node:SphereNode){
        self.nodes[node.id] = node
    }
    
    func printEdges(){
        for e in edges{
            println(e)
        }
    }
    
    func printNodes(){
        for n in nodes{
            println(n)
        }
    }
}