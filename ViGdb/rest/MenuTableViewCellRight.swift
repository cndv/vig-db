// MenuTableViewCellRight.swift
//
// Konstantinos Chartomatzis
// Cell class
import UIKit

class MenuTableViewCellRight: UITableViewCell {
    @IBOutlet weak var label2: UILabel!
    
    override func awakeFromNib() {
        self.label2.textAlignment = .Right
        super.awakeFromNib()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}