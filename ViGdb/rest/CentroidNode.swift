//
//  CentroidNode.swift
//
//
// Konstantinos Chartomatzis
//
// Model for clustered Node
//


import Foundation
import SceneKit

class CentroidNode : SCNScene {
    var id: Int
    var node: SCNNode
    var x: Float
    var y: Float
    var z: Float
    var nodeTypes: [String]
    var nodes: [SphereNode]
    
    init (id: Int, x:Float, y: Float, z:Float, nodeTypes:[String], nodes: [SphereNode]){
        self.id = id
        self.node = SCNNode()
        self.x = x
        self.y = y
        self.z = z
        self.nodeTypes = nodeTypes
        self.nodes = nodes
        super.init()
        
    }
    
    func getWeightOfNode() -> Int {
        return self.nodes.count
    }

    override var description: String {
        return ("id is \(id), x is \(x), y is \(y), z is \(z), nodeTypes are \(nodeTypes), nodes are , \(nodes)")
    }
    
    func renderer(aRenderer: SCNSceneRenderer, willRenderScene scene: SCNScene, atTime time: NSTimeInterval) {
        //Makes the lines thicker
        glLineWidth(20)
    }
    
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}