//
//  SphereNode.swift
//
// Konstantinos Chartomatzis
//
// Sphere Node model
//

import Foundation
import SceneKit

class SphereNode : SCNScene {
    var id: Int
    var name: String
    var node: SCNNode
    var x: Float
    var y: Float
    var z: Float
    var nodeType: String
    var classType: Int
    
    init (id: Int, name : String, nodeType: String, x: Float, y:Float, z:Float, classType: Int){
        self.id = id
        self.name = name
        self.x = x
        self.y = y
        self.z = z
        self.nodeType = nodeType
        self.classType = classType
        
        self.node = SCNNode()
        super.init()
        
    }
    
    
    init (id:Int, x:Float, y:Float, z:Float) {
        self.id = id
        self.x = x
        self.y = y
        self.z = z
        self.name = ""
        self.nodeType = ""
        self.classType = 0
        self.node = SCNNode()
        
        super.init()
    }
    
  
    func renderer(aRenderer: SCNSceneRenderer, willRenderScene scene: SCNScene, atTime time: NSTimeInterval) {
        //Makes the lines thicker
        glLineWidth(20)
    }
    
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}