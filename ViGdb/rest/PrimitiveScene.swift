//
//  PrimitivesScene.swift
//
//  Adds geometry shapes to the view, spheres, cylinders etc.
//
//
//  Konstantinos Chartomatzis

import UIKit
import SceneKit
import SpriteKit


class PrimitivesScene:  SCNScene {
    
    override init() {
        super.init()
        }
    
    
    func addSphere(withRadius: CGFloat, atX: Float, atY: Float, atZ: Float, withColor: UIColor, withLabel: String) -> SCNNode {
        let sphereGeometry = SCNSphere(radius: withRadius)
//        sphereGeometry.segmentCount = 6 // segment // default 48
        sphereGeometry.firstMaterial?.diffuse.contents = withColor
        sphereGeometry.firstMaterial?.specular.contents = UIColor.whiteColor()
        sphereGeometry.firstMaterial?.ambient.contents = UIColor.whiteColor()
        let sphereNode = SCNNode(geometry: sphereGeometry)
        sphereNode.position = SCNVector3(x: atX, y: atY, z: atZ)
        self.rootNode.addChildNode(sphereNode)
        return sphereNode
    }
    
    
    func addCylinderEdgeBetweenNodes(nodeA:SCNNode, nodeB: SCNNode, withRadius: CGFloat, withColor: UIColor) -> SCNNode{
        // height of the cylinder should be the distance between points
        let height = CGFloat(GLKVector3Distance(SCNVector3ToGLKVector3(nodeA.position), SCNVector3ToGLKVector3(nodeB.position)))
        
        // add a container node for the cylinder to make its height run along the z axis
        let zAlignNode = SCNNode()
        zAlignNode.eulerAngles.x = Float(M_PI_2)
        // and position the zylinder so that one end is at the local origin
        let cylGeometry = SCNCylinder(radius: withRadius, height: height)
        cylGeometry.radialSegmentCount = 3

        let cylinder = SCNNode(geometry: cylGeometry)
        cylinder.geometry?.firstMaterial?.diffuse.contents = withColor
//        cylinder.geometry?.firstMaterial?.specular.contents = UIColor.whiteColor()
        cylinder.geometry?.firstMaterial?.ambient.contents = withColor
        cylinder.position.y = Float(-height/2)
        zAlignNode.addChildNode(cylinder)
        
        // put the container node in a positioning node at one of the points
        var p2Node = SCNNode()
        var p1Node = SCNNode()
//        println(nodeA.position)
//        println(nodeB.position)
        p1Node.position = nodeA.position
        p2Node.position = nodeB.position
        
        p2Node.addChildNode(zAlignNode)
        // and constrain the positioning node to face toward the other point
        p2Node.constraints = [ SCNLookAtConstraint(target: p1Node) ]
        self.rootNode.addChildNode(p2Node)
        return p2Node
    }
    
    func addEdgeBetweenNodes(nodeA: SCNNode, nodeB: SCNNode, withColor: UIColor, withWidth: Float) {
        let line = lineBetweenNodeA(nodeA, nodeB: nodeB, withColor: withColor, withWidth: withWidth)
        self.rootNode.addChildNode(line)
    }

    
    func lineBetweenNodeA(nodeA: SCNNode, nodeB: SCNNode, withColor: UIColor, withWidth: Float) -> SCNNode {
        let positions: [Float32] = [nodeA.position.x,
            nodeA.position.y,
            nodeA.position.z,
            nodeB.position.x,
            nodeB.position.y,
            nodeB.position.z]
        let positionData = NSData(bytes: positions, length: sizeof(Float32)*positions.count)
        let indices: [Int32] = [0, 1]
        let indexData = NSData(bytes: indices, length: sizeof(Int32) * indices.count)
        
        let source = SCNGeometrySource(data: positionData,
                                        semantic: SCNGeometrySourceSemanticVertex,
                                        vectorCount: indices.count,
                                        floatComponents: true,
                                        componentsPerVector: 3,
                                        bytesPerComponent: sizeof(Float32),
                                        dataOffset: 0,
                                        dataStride: sizeof(Float32) * 3)
        
        let element = SCNGeometryElement(data: indexData,
                                        primitiveType: SCNGeometryPrimitiveType.Line,
                                        primitiveCount: indices.count,
                                        bytesPerIndex: sizeof(Int32))
        
        let line = SCNGeometry(sources: [source], elements: [element])
        line.firstMaterial?.lightingModelName = SCNLightingModelConstant
        line.firstMaterial?.diffuse.contents = withColor

        //width
//        glLineWidth(withWidth)
        
        return SCNNode(geometry: line)
    }

    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
}
