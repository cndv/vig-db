//
//  ColorManager.swift
//
//  Created by Kostas Chart on 18/08/2015.
//
//  Responsible for handling the colors of the graph
//

import Foundation
import UIKit

class ColorModel {
    var nodeTypeColor: [String:UIColor]
    var edgeColor: [String:UIColor]
    var classColor: [Int:UIColor]
    var colors: [UIColor]
    var centralities : [String:UIColor]
    
    init(){
        nodeTypeColor = [String:UIColor]()
        edgeColor = [String:UIColor]()
        classColor = [Int:UIColor]()
        colors = [UIColor]()
        centralities = [String:UIColor]()
        self.initUniqueColors()
    }

    func initUniqueColors(){
        colors.append(UIColor(red: 0.8821, green: 0.8779, blue: 0.8862, alpha: 1.0))
                colors.append(UIColor(red: 0.0706, green: 0.0824, blue: 0.0745, alpha: 1.0))
                colors.append(UIColor(red: 0.8667, green: 0.749, blue: 0.2196, alpha: 1.0))
                colors.append(UIColor(red: 0.302, green: 0.1137, blue: 0.3804, alpha: 1.0))
                colors.append(UIColor(red: 0.7373, green: 0.3098, blue: 0.1373, alpha: 1.0))
                colors.append(UIColor(red: 0.5216, green: 0.7333, blue: 0.8078, alpha: 1.0))
                colors.append(UIColor(red: 0.5804, green: 0.0745, blue: 0.1608, alpha: 1.0))
                colors.append(UIColor(red: 0.6627, green: 0.6627, blue: 0.4196, alpha: 1.0))
                colors.append(UIColor(red: 0.3843, green: 0.4, blue: 0.3882, alpha: 1.0))
                colors.append(UIColor(red: 0.3098, green: 0.5647, blue: 0.2157, alpha: 1.0))
                colors.append(UIColor(red: 0.7216, green: 0.4, blue: 0.5725, alpha: 1.0))
                colors.append(UIColor(red: 0.2118, green: 0.349, blue: 0.5569, alpha: 1.0))
                colors.append(UIColor(red: 0.7608, green: 0.4118, blue: 0.3059, alpha: 1.0))
                colors.append(UIColor(red: 0.1765, green: 0.1373, blue: 0.4157, alpha: 1.0))
                colors.append(UIColor(red: 0.7882, green: 0.5373, blue: 0.1686, alpha: 1.0))
                colors.append(UIColor(red: 0.4157, green: 0.098, blue: 0.3961, alpha: 1.0))
                colors.append(UIColor(red: 0.8667, green: 0.8902, blue: 0.2941, alpha: 1.0))
                colors.append(UIColor(red: 0.3176, green: 0.0784, blue: 0.0863, alpha: 1.0))
                colors.append(UIColor(red: 0.4706, green: 0.5922, blue: 0.1882, alpha: 1.0))
                colors.append(UIColor(red: 0.2863, green: 0.1412, blue: 0.0902, alpha: 1.0))
                colors.append(UIColor(red: 0.6745, green: 0.1137, blue: 0.1294, alpha: 1.0))
                colors.append(UIColor(red: 0.1137, green: 0.1451, blue: 0.0902, alpha: 1.0))
        
        centralities["central"] = UIColor(red: 1.0, green: 0.4, blue: 0.4, alpha: 1.0)
        centralities["average"] = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1.0)


    }
    
    func getColor() -> UIColor {

        var color = UIColor()
        var numberOFColors = UInt32(colors.count)
        if numberOFColors > 0 {
            var randomIndex =  arc4random_uniform(numberOFColors)
            color = colors.removeAtIndex(Int(randomIndex))
        }
        else{
            color = randomColor(hue: .Random, luminosity: .Bright)
        }
        return color
    }
    
    
    
}