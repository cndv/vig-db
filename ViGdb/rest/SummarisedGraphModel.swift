//
//  SummarisedGraphModel.swift
//
// Konstantinos Chartomatzis
// Graph model for summarised data

import Foundation


class SummarisedGraphModel {
    
    var edges : [CentroidRelation]
    var clusters : [Int: CentroidNode] // id : centroidnode
    
    init() {
        // perform CentroidRelation initialization here
        edges = [CentroidRelation]()
        clusters = [Int: CentroidNode]()
    }
    
    func addEdge(e: CentroidRelation){
        self.edges.append(e)
    }
    
    func addNode(node:CentroidNode){
        self.clusters[node.id] = node
    }
    
    func printEdges(){
        for e in edges{
            println(e)
        }
    }
    
    func printNodes(){
        for n in clusters{
            println(n)
        }
    }
}