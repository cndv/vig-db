//
//  Relation.swift
//
// Konstantinos Chartomatzis
// Relation model

import Foundation

class Relation {
    var source: Int
    var target: Int
    var type: String
    var weight: Int
    
    init (source: Int, target: Int, type: String){
        self.source = source
        
        self.target = target
        self.type = type
        self.weight = 0
    }           
}