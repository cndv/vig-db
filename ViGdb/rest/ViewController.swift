//
//  ViewController.swift
//
//  Konstantinos Chartomatzis
//
//  Intializes the screen, handles gestures, and contain all other controllers
//

import UIKit
import SceneKit
import SpriteKit
import PKHUD

class ViewController: UIViewController,MenuTransitionManagerDelegate {
    var clusteringAlg = "Community Detection"
    var layoutAlg = "Force Directed Layout"
    private var menuTransitionManager = MenuTransitionManager()
    var colorMngr = ColorModel()
    var sumModel = SummarisedGraphModel()
    var sumGH = SummarisedGraphHandler()
    var sumDH = SummarisedDrawHandler ()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Graph"

        self.displayGraph()
    }
    
    func pickClusteringAlg() -> Clustering {
        //pick clustering algorithm
        switch self.clusteringAlg {
        case "Community Detection":
            return Clustering.Community
        case "Betweenness Centrality":
            return Clustering.Centrality
        case "Vertex Type":
            return Clustering.NodeType
        case "Only Edges":
            return Clustering.Edges
        case "Graph Summary":
            return Clustering.Summary
        default:
            return Clustering.Community
        }
    }
    
    func pickLayoutAlg() -> Layout {
        switch self.layoutAlg {
        case "Spectral Layout":
            return Layout.Spectral
        case "Force Directed Layout":
            return Layout.ForceDirected
        default:
            return Layout.ForceDirected
        }
    }
    
    func displayGraph(){
        //
        //load scenekit
        //
        self.view.clearsContextBeforeDrawing = true
        let scnView = self.view as! SCNView
        scnView.scene = PrimitivesScene()
        scnView.backgroundColor = UIColor ( red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0 )
        scnView.autoenablesDefaultLighting = true
        scnView.allowsCameraControl = true
//        scnView.playing = true
//        scnView.showsStatistics = true

        //
        // get data
        //
        var clustering = self.pickClusteringAlg()
        var layout = self.pickLayoutAlg()
        if clustering != .Summary {
            self.getFullGraph(clustering, layout: layout, scnView: scnView)
        }
        else {
            self.getSummarisedGraph(clustering, layout: layout, scnView: scnView)
        }
    }
    
    func getSummarisedGraph(clustering: Clustering, layout: Layout, scnView: SCNView) {
        var rh = RestHandler()
        rh.clustering = clustering
        rh.layout = layout
        // pick layout algorithms
        var sgh = SummarisedGraphHandler()
        var sdh = SummarisedDrawHandler()
        sdh.drawingMethod = self.pickClusteringAlg()
        sgh.colorMngr = self.colorMngr
//        self.sumDH = SummarisedDrawHandler()
        
        rh.getData(Quantity.SummarisedGraph, completion: { (data) -> Void in
            sgh.parseData(data)
            sdh.summarisedGraph = sgh.sumGraph
            sdh.scene = scnView.scene as! PrimitivesScene
            sdh.colorMngr = sgh.colorMngr
            scnView.overlaySKScene = HudScene(size: scnView.frame.size, colorManager: sdh.colorMngr)// hud
            
            self.sumDH.summarisedGraph = sgh.sumGraph
            self.sumDH.classColor = sdh.classColor
            self.sumDH.scene = sdh.scene
            self.sumDH.colorMngr = sgh.colorMngr

            let tapGesture = UITapGestureRecognizer(target: self, action: "handleTap:")
            
            var gestureRecognizers = [AnyObject]()
            gestureRecognizers.append(tapGesture)
            if let existingGestureRecognizers = scnView.gestureRecognizers {
                gestureRecognizers.extend(existingGestureRecognizers)
            }
            scnView.gestureRecognizers = gestureRecognizers
            
            sdh.drawGraph()
        })
    }
    
  
    
    func handleTap(gestureRecognize: UIGestureRecognizer) {
        // retrieve the SCNView
        let scnView = self.view as! SCNView
        
        // check what nodes are tapped
        let p = gestureRecognize.locationInView(scnView)
        if let hitResults = scnView.hitTest(p, options: nil) {
            // check that we clicked on at least one object
            if hitResults.count > 0 {
                // retrieved the first clicked object
                let result: AnyObject! = hitResults[0]
                // get its material
                let material = result.node!.geometry!.firstMaterial!
                // make it transparent
                // highlight it
                SCNTransaction.begin()
                SCNTransaction.setAnimationDuration(0.5)
                
                // on completion - unhighlight
                SCNTransaction.setCompletionBlock {
                    SCNTransaction.begin()
                    SCNTransaction.setAnimationDuration(0.5)
                    material.emission.contents = UIColor.blackColor()
                    SCNTransaction.commit()
                }
                material.emission.contents = UIColor ( red: 0.0, green: 0.502, blue: 1.0, alpha: 0.67 )
                SCNTransaction.commit()
                var position = hitResults[0].node!.position
                self.sumDH.findClusterTapped(position)
                if result.node!.geometry is SCNSphere{
                    material.transparency = 0.4
                }

            }
        }
    
    }
    
    
    func getFullGraph(clustering: Clustering, layout: Layout, scnView: SCNView) {
        var rh = RestHandler()
        rh.clustering = clustering
        rh.layout = layout
        // pick layout algorithms
        
        var gh = GraphHandler()
        var dh = DrawHandler()
        dh.drawingMethod = clustering
        gh.colorMngr = self.colorMngr
        gh.clustering = clustering        

        rh.getData(Quantity.CompleteGraph, completion: { (data) -> Void in
        
            gh.parseData(data)

            dh.graph = gh.graph
            dh.scene = scnView.scene as! PrimitivesScene
            dh.colorMngr = gh.colorMngr
            if clustering == .Centrality {
                dh.centralities = gh.centralities
            }
            // display Hud with infor about edges and nodes regarding their colors
            scnView.overlaySKScene = HudScene(size: scnView.frame.size, colorManager: dh.colorMngr) // hud
            dh.drawGraph()
        })
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showMenu" {
            let menuTableViewController = segue.destinationViewController as! MenuTableViewController
            menuTableViewController.currentItem = self.title!
            
            // Assign animator
            self.menuTransitionManager.delegate = self
            self.menuTransitionManager.animateDirection = 1
            menuTableViewController.transitioningDelegate = self.menuTransitionManager
        }
        else if segue.identifier == "showMenuRight" {
            let menuTableViewController = segue.destinationViewController as! MenuTableViewControllerRight
            menuTableViewController.currentItem = self.title!
            
            // Assign animator
            self.menuTransitionManager.delegate = self
            self.menuTransitionManager.animateDirection = 0
            menuTableViewController.transitioningDelegate = self.menuTransitionManager
        }
    }
    
    func dismiss() {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func unwindToHome(segue: UIStoryboardSegue) {
        if let sourceController = segue.sourceViewController as? MenuTableViewController {
            self.clusteringAlg = sourceController.currentItem
            println("clustering alg is \(self.clusteringAlg )")
        }
        else if let sourceController = segue.sourceViewController as? MenuTableViewControllerRight {
            self.layoutAlg = sourceController.currentItem
        }
        self.title = self.clusteringAlg+", with "+self.layoutAlg
        self.displayGraph()
    }
       
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
}