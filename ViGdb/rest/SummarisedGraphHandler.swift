//
//  SummarisedGraphHandler.swift
// Konstantinos Chartomatzis
//  Summarised Data ready for drawing


import Foundation
import UIKit



extension String {
    var floatValue: Float {
        return (self as NSString).floatValue
    }
}

class SummarisedGraphHandler {
    var sumGraph: SummarisedGraphModel
    var colorMngr = ColorModel()
    var nodeTypeColor: [String:UIColor]
    var edgeColor: [String:UIColor]
    var randColor = Set<UIColor>()

    
    init (){
        self.sumGraph = SummarisedGraphModel()
        self.nodeTypeColor = [String:UIColor]()
        self.edgeColor = [String:UIColor]()
    }
    
    func addEdgeColor(type: String) {
        if colorMngr.edgeColor[type] == nil  {
            var rcolor = colorMngr.getColor()
            colorMngr.edgeColor[type] = rcolor
        }
    }
    
    func parseNodes(nodes:Array<Dictionary<String, String>>) -> [SphereNode] {
        var returnNodes = [SphereNode]()
        
        for n in nodes {
            var nd = SphereNode(id: n["id"]!.toInt()!, x: n["x"]!.floatValue, y: n["y"]!.floatValue, z: n["z"]!.floatValue)
            returnNodes.append(nd)
        }
        
        return returnNodes
    }
    
    
    func createCentroidRelation(cluster_id: Int, values: JSON) -> CentroidRelation{
        
        var source = cluster_id
        var edge_types = values["edge_types"].rawValue as! NSArray
        var targets = values["targets"].rawValue as! NSArray
        var weight = (values["weight"].rawValue).floatValue

        var r = CentroidRelation(source: source, targets: targets as! [Int], edge_types: edge_types as! [String], weight: weight)
        self.addEdgeColor(edge_types[0] as! String) // MARK: TODO update to combine the colors 
        return r
    }

    func createCentroidNode(cluster_id: Int, values: JSON) -> CentroidNode {
        var source = cluster_id
        var node_types = (values["node_types"].rawValue as! NSArray) as! [String]
//        var nodes = (values["nodes"].rawValue as! NSArray)
        var tnodes = values["nodes"].rawValue as! Array<Dictionary<String, String>>
        
        var parsedNodes = self.parseNodes(tnodes)
        
//        println(parsedNodes)
        
        var x = values["x"].floatValue
        var y = values["y"].floatValue
        var z = values["z"].floatValue
        var n = CentroidNode(id: source, x: x, y: y, z: z, nodeTypes: node_types, nodes: parsedNodes)
//        println(n)
        return n
    }
    
    func parseData(data: AnyObject){
        var g = JSON(data)
        var edges = g["cluster_links"]
        var nodes = g["cluster_centroids"]
        var randColor = Set<UIColor>()
        println("info about graph" )
        println("Summary: Number of nodes: \(nodes.count)")
        println("Summary: Number of edges \(edges.count)")

        
        //
        // parse nodes
        //
        for (cluster_id, values) in nodes {
            var n = self.createCentroidNode(cluster_id.toInt()!, values:values)
            self.sumGraph.addNode(n)
        }
        
        //
        // parse edges
        //
        for (cluster_id, values) in edges {
            var r = self.createCentroidRelation((cluster_id as String).toInt()!, values: values)
            self.sumGraph.addEdge(r)
        }

           }
    
    func randomStringWithLength (len : Int) -> NSString {
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var randomString : NSMutableString = NSMutableString(capacity: len)
        
        for (var i=0; i < len; i++){
            var length = UInt32 (letters.length)
            var rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.characterAtIndex(Int(rand)))
        }
        return randomString
    }    
    
    func getRandomColor ()-> UIColor {
        return randomColor(hue: .Random, luminosity: .Bright)
    }
    
}
