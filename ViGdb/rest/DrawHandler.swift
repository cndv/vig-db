//
//  ForceLayout.swift
//
//  Konstantinos Chartomatzis
//
//  Handles the grpah data, that are ready for drawing
//

import Foundation
import UIKit
import SceneKit


enum DrawMethod {
    case Clustering
    case NodeType
    case Edges
    case Summary
}

class DrawHandler{
    var graph : GraphModel
    var scene : PrimitivesScene
    var nodes : [Int:SCNNode]
    var typeColor: [String:UIColor]
    var edgeColor: [String:UIColor]
    var classColor: [Int:UIColor]
    var drawingMethod : Clustering
    var colorMngr =  ColorModel()
    var centralities : [Int:Double]


    init(){
        self.graph = GraphModel()
        self.scene = PrimitivesScene()
        self.nodes = [Int:SCNNode]()
        self.typeColor = [String:UIColor]()
        self.edgeColor = [String:UIColor]()
        self.classColor = [Int:UIColor]()
        self.drawingMethod = .Edges
        self.centralities = [Int:Double]()
    }
    
    func drawGraph(){
        self.drawNodes()
        self.drawEdges()
        self.drawCentralities()
    }

    func drawCentralities() {
        var radius = CGFloat(0.02)
        for (k,v) in self.centralities {
            var n = self.graph.nodes[k]
            self.scene.addSphere(radius, atX: n!.x, atY: n!.y, atZ: n!.z,
                withColor:colorMngr.centralities["central"]!, withLabel: "")
        }
    }
    
    var nodeCount = 0
    func drawNodes(){
        var color = UIColor()
        var radius = CGFloat(0)
        for n in graph.nodes {
            var node : SCNNode

            if drawingMethod == Clustering.Community {
                color = colorMngr.classColor[n.1.classType]!
                radius = 0.005
            }
            else if drawingMethod == .NodeType {
                color = colorMngr.nodeTypeColor[n.1.nodeType]!
                radius = 0.005
            }
            else if drawingMethod == .Edges {
                radius = 0.0005
                color = colorMngr.nodeTypeColor[n.1.nodeType]!
            }
            else if drawingMethod == .Centrality {
                radius = 0.005
                color = colorMngr.centralities["average"]!
                self.drawCentralities()
            }
            var name = n.1.name
            var clType = n.1.classType
    
      
            node = self.scene.addSphere(radius, atX: n.1.x, atY: n.1.y, atZ: n.1.z,
            withColor:color, withLabel: name)
            n.1.node = node // add to model
            nodes[n.1.id] = node
            nodeCount++
      
            
        }
        println("Completed NodeCount is : \(nodeCount)")
    }
   
    var edgeCount = 0
    func drawEdges(){
        for e in graph.edges {
            var color = colorMngr.edgeColor[e.type]
            if let s = nodes[e.source]  {
                if let t = nodes[e.target] {
                    self.scene.addEdgeBetweenNodes( s,nodeB: t, withColor: color!, withWidth: 2.0)
                edgeCount++
                }
            }
        }
        println("Complted EdgeCount is : \(edgeCount)")
    }
    
    
    
}