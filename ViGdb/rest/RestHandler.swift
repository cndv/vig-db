//
//  RestHandler.swift
//
//  Konstantinos Chartomatzis
//
//  Handles all the communication with the web service
//

import Foundation
import UIKit


enum Clustering {
    case Community
    case NodeType
    case Centrality
    case Edges
    case Summary
}

enum Layout {
    case Spectral
    case ForceDirected
}

enum Quantity {
    case CompleteGraph
    case SummarisedGraph
}
class RestHandler {
    
    var g : GraphModel
    var clustering: Clustering
    var layout : Layout
    
    init (){
        self.g = GraphModel()
        self.clustering = .Community
        self.layout = .Spectral
    }
            
    func buildURL(method: String) -> String {
        var baseURL = "http://localhost:5000/"
        var baseURLProduction = "http://192.168.0.3:5000/"
        var deployedURL = "http://nskostas.pythonanywhere.com/"
        
        var url = String()
        
        // change this for local or deployed server
        url += deployedURL + method
        
        if self.clustering == .Community {
            url += "cluster=community"
        }
        else if self.clustering == .Centrality {
            url += "cluster=centrality" //MARK: todo centrality
        }
        else if self.clustering == .NodeType {
            url += "cluster=nodetype"
        }
        else if self.clustering == .Edges {
            url += "cluster=edges"
        }
        
        if self.layout == .ForceDirected {
            url += "&layout=spring"
        }
        else if self.layout == .Spectral {
            url += "&layout=spectral"
        }
        println(url)
        return url
    }
    
    //
    //rest
    //
    func getData(quantityOfData: Quantity, completion: (data: AnyObject)-> Void){
        
//        let user = "movies"
//        let password = "MhMrElgHxmUjpJwJ8ABD"
        
        let session = NSURLSession.sharedSession()
        var urlString = String()
        if quantityOfData == .CompleteGraph {
            urlString = self.buildURL("graphdata?")
        }
        else if quantityOfData == .SummarisedGraph {
            urlString = self.buildURL("graphsummary?")
        }
        
//        let urlString = "http://0.0.0.0:5000/graphdata?cluster=community&layout=spectral"
//        let urlString = "http://localhost:5000/graph"

        
        let url = NSURL(string: urlString)!
        println(url)
        let request = NSURLRequest(URL: url)

        let task = session.dataTaskWithRequest(request) {data, response, downloadError in

            if let error = downloadError {
                println("Could not complete the request \(error)")
            } else {
                var parsingError: NSError? = nil
                dispatch_async(dispatch_get_main_queue(), {
                    let dataDown: AnyObject! = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &parsingError)
                    completion(data: dataDown)
                })
            }
        }
        task.resume()
    }
}