//
//  HudScene.swift
//  rest
//
//  Konstantinos Chartomatzis

// Provides HUD Information for nodes ( Such as Color of Nodes and Edges)

import Foundation
import SpriteKit

class HudScene: SKScene {
    var colorMngr = ColorModel()

    var clusters: [String: UIColor]
    
    init(size: CGSize, colorManager: ColorModel){
        self.colorMngr = colorManager
        self.clusters = [String:UIColor]()
        super.init(size: size)
        self.setupHud()
        
    }
    
    
    func addEdgeInfo(label: String, color: UIColor, heightDifference: CGFloat){
        // label 1
        // label should not be more than 5 chars
        let edgeLabel = SKLabelNode(fontNamed: "Courier")
        edgeLabel.name = "random"
        edgeLabel.fontSize = 15
        edgeLabel.fontColor = color
        edgeLabel.text = String(format: label, 0)
        edgeLabel.position = CGPointMake(self.size.width-100,
            self.size.height-200 + heightDifference)
        self.addChild(edgeLabel)
        
        // line 1
        var line = SKShapeNode()
        var path = CGPathCreateMutable()
        CGPathMoveToPoint(path, nil, self.size.width-55, self.size.height-200 + heightDifference)
        CGPathAddLineToPoint(path, nil, self.size.width-5, self.size.height-200 + heightDifference)
        line.path = path
        line.fillColor = color
        line.strokeColor = color
        line.lineWidth = 0.5
        line.glowWidth = 0.5
        self.addChild(line)
        
    }
    
    func addNodeHudInfo(label: String, color: UIColor, heightDifference: CGFloat){
        // label 1
        let nodeLabel = SKLabelNode(fontNamed: "Courier")
        nodeLabel.fontSize = 15
        nodeLabel.fontColor = color
        nodeLabel.text = String(format: label, 0)
        nodeLabel.position = CGPointMake(65,self.size.height - 102 - heightDifference)
        self.addChild(nodeLabel)
        
        // circle 1
        var Circle = SKShapeNode(circleOfRadius: 8) // Size of Circle
        Circle.position = CGPointMake(20, self.size.height - 100 - heightDifference)  //Middle of Screen
        Circle.strokeColor = color
//        Circle.fillColor = color
        Circle.glowWidth = 0.01
        self.addChild(Circle)
    }
    
    
    func setupHud() {
        // show info about edges
        var hDiff = CGFloat(0)
        for (label, color) in colorMngr.edgeColor {
            self.addEdgeInfo(label, color: color, heightDifference: hDiff)
            hDiff = hDiff + CGFloat(20)
        }
        
        // show info about nodes
        hDiff = CGFloat(0)
        var colorsDict = colorMngr.classColor
        let sortedKeys = Array(colorsDict.keys).sorted(<) // ["A", "D", "Z"]
        
        for c_id in sortedKeys {
            var strC = String(c_id)
            self.addNodeHudInfo("Class \(strC)", color: colorsDict[c_id]!, heightDifference: hDiff)
            hDiff = hDiff + CGFloat(30)
        }
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}