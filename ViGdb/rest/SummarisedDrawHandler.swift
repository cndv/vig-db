//
//  SummarisedDrawHandler.swift
//
//  Konstantinos Chartomatzis
//
//  Summary data ready for drawing
//

import Foundation
import UIKit
import SceneKit
import PKHUD

enum ColorType {
    case TypeColor
    case EdgeColor
    case ClassColor
}

// in summary cl
extension UIColor {
    static func randomColor() -> UIColor {
        let r = randomCGFloat()
        let g = randomCGFloat()
        let b = randomCGFloat()
        return UIColor(red: r, green: g, blue: b, alpha: 1.0)
    }
}

class SummarisedDrawHandler{
    var summarisedGraph: SummarisedGraphModel
    var scene : PrimitivesScene
    var clusters : [Int:SCNNode]
    var edges: [SCNNode]
    var typeColor: [String:UIColor]
    var edgeColor: [String:UIColor]
    var classColor: [Int:UIColor]
    var randColor = Set<UIColor>()
    var drawingMethod : Clustering
    var colorMngr = ColorModel()
    
    
    init(){
        self.summarisedGraph = SummarisedGraphModel()
        self.scene = PrimitivesScene()
        self.clusters = [Int:SCNNode]()
        self.typeColor = [String:UIColor]()
        self.edgeColor = [String:UIColor]()
        self.classColor = [Int:UIColor]()
        self.drawingMethod = .Edges
        self.edges = [SCNNode]()
    }
    
    func drawGraph(){
        self.drawNodes()
        self.drawEdges()
    }
    
    var nodeCount = 0
    func drawNodes(){
        var color = UIColor()
        var radius = CGFloat(0)
        for (id,values) in summarisedGraph.clusters {
            // set color
            var rcolor = colorMngr.classColor[id]

            // set radius
            var radius = 0.005
            var countNodes =  values.getWeightOfNode()
            radius =  radius + (Double(countNodes) * 0.001)
            
            // create sphere
            var node = SCNNode()
            node = self.scene.addSphere(CGFloat(radius), atX: values.x, atY: values.y, atZ: values.z, withColor: rcolor!, withLabel: id.description)
            
            // add to model
            self.clusters[id] = node
            nodeCount++
        }
        println("Nodes added to the scene are: \(nodeCount)")
    }
    
    func drawEdges(){
        for e in summarisedGraph.edges {
            var color = colorMngr.edgeColor[e.edge_types[e.edge_types.count-1]]
            if let s = self.clusters[e.source]  {
                for tt in e.targets {
                    if let t = self.clusters[tt] {
                        let radius = CGFloat(0.002*e.weight/5.0)
//                        self.scene.addEdgeBetweenNodes( s,nodeB: t, withColor: color!, withWidth: e.weight*10.0 )
                        self.scene.addCylinderEdgeBetweenNodes(s, nodeB: t, withRadius: radius, withColor: color!)
                    }
                }
            }
        }
    }
    
    func drawNode (position: SCNVector3, withColor: UIColor){
        self.scene.addSphere(CGFloat(0.005), atX: position.x, atY: position.y, atZ: position.z, withColor: withColor, withLabel: "")
    }
    
    func showInfoAboutNode(message: String){
        PKHUD.sharedHUD.contentView = PKHUDTextView(text: message)
        PKHUD.sharedHUD.show()
        PKHUD.sharedHUD.hide(afterDelay: 2.0)
    }
    
    func findClusterTapped(position: SCNVector3) {
//        var cluster = CentroidNode()
        var cluster_tapped = -1
        for (c_id, n) in self.summarisedGraph.clusters {
            if n.x == position.x && n.y == position.y && n.z == position.z {
                cluster_tapped = c_id
                break
            }
        }
        // get the cluster
        if cluster_tapped != -1 {
            var cluster = self.summarisedGraph.clusters[cluster_tapped]
            var str_nodes = "Nodes are: "
            for (n) in cluster!.nodes {
                str_nodes += n.id.description+", "
            }
            // draw its children
            self.showInfoAboutNode("Cluster ID: "+String(cluster_tapped)+"\n"+str_nodes)
            for n in cluster!.nodes {
                var p = SCNVector3(x: n.x, y: n.y, z: n.z)
                drawNode(p, withColor: self.colorMngr.classColor[cluster_tapped]!)
            }
        }
//        return cluster
    }
    
}