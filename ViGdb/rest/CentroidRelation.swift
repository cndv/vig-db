//
//  CentroidRelation.swift
//
// Konstantinos Chartomatzis

// Model for clustered relation 

import Foundation

class CentroidRelation {
    var source: Int
    var targets: [Int]
    var edge_types: [String]
    var weight: Float
    
    init (source: Int, targets: [Int], edge_types: [String], weight: Float){
        self.source = source
        self.targets = targets
        self.edge_types = edge_types
        self.weight = weight
    }
}