//
//  MenuTransitionManager.swift
// 
// Konstantinos Chartomatzis
// Handling transitions and animation

import UIKit

@objc protocol MenuTransitionManagerDelegate {
    func dismiss()
}

class MenuTransitionManager: NSObject, UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate {
    
    var duration = 0.5
    var isPresenting = false
    var delegate:MenuTransitionManagerDelegate?
    var animateDirection = 0 // 1 -> left, 0 -> Right
    
    var snapshot:UIView? {
        didSet {
            if let _delegate = delegate {
                let tapGestureRecognizer = UITapGestureRecognizer(target: _delegate, action: "dismiss")
                snapshot?.addGestureRecognizer(tapGestureRecognizer)
            }
        }
    }
    
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning) -> NSTimeInterval {
        return duration
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        
        // Get reference to our fromView, toView and the container view
        let fromView = transitionContext.viewForKey(UITransitionContextFromViewKey)!
        let toView = transitionContext.viewForKey(UITransitionContextToViewKey)!
        
        // Set up the transform for sliding
        let container = transitionContext.containerView()
        var moveRight = CGAffineTransformMakeTranslation(-500, 0) // useless
        var moveLeft = CGAffineTransformMakeTranslation(300, 0) // useless
        if self.animateDirection == 1 { // left menu
            moveRight = CGAffineTransformMakeTranslation(300, 0)
            moveLeft = CGAffineTransformMakeTranslation(-200, 0)
        }
        else { // right menu
            moveRight = CGAffineTransformMakeTranslation(-200, 0)
            moveLeft = CGAffineTransformMakeTranslation(300, 0)
        }
//        println(" direction is \(self.animateDirection)")
        
        // Add both views to the container view
        if isPresenting {
            toView.transform = moveLeft
            snapshot = fromView.snapshotViewAfterScreenUpdates(true)
            container.addSubview(toView)
            container.addSubview(snapshot!)
        }
        
        // Perform the animation
        UIView.animateWithDuration(duration, delay: 0.0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.3, options: nil, animations: {
            
            if self.isPresenting {
                self.snapshot?.transform = moveRight
                toView.transform = CGAffineTransformIdentity
            } else {
                self.snapshot?.transform = CGAffineTransformIdentity
                fromView.transform = moveLeft
            }
            
            
            }, completion: { finished in
                
                transitionContext.completeTransition(true)
                if !self.isPresenting {
                    self.snapshot?.removeFromSuperview()
                }
        })
        
    }
    
    
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        isPresenting = false
        return self
    }
    
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        isPresenting = true
        return self
    }
    
    
}
