//
//  MenuTableViewController.swift
//
// Konstantinos Chartomatzis
// Table view for storing options

import UIKit

class MenuTableViewController: UITableViewController {
    var menuItems = ["Community Detection","Vertex Type", "Betweenness Centrality", "Only Edges", "Graph Summary"]
    var currentItem = "Community Detection"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return menuItems.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! MenuTableViewCell
        
        // Configure the cell...
        cell.label.text = menuItems[indexPath.row]
        cell.label.textColor = (menuItems[indexPath.row] == currentItem) ? UIColor.blueColor() : UIColor.grayColor()
        
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let menuTableViewController = segue.sourceViewController as! MenuTableViewController
        if let selectedRow = menuTableViewController.tableView.indexPathForSelectedRow()?.row {
            currentItem = menuItems[selectedRow]
        }
    }
    
}
